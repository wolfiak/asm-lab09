.386
.MODEL FLAT, STDCALL

include grafika.inc


WNDstyle EQU WS_CLIPCHILDREN + WS_OVERLAPPEDWINDOW + WS_HSCROLL + WS_VSCROLL
CSstyle		EQU		CS_HREDRAW + CS_VREDRAW + CS_GLOBALCLASS
WNDstyle	EQU		WS_CLIPCHILDREN + WS_OVERLAPPEDWINDOW + WS_HSCROLL+WS_VSCROLL

ExitProcess PROTO : DWORD
LoadIconA PROTO :DWORD , :DWORD
LoadCursorA PROTO :DWORD, :DWORD
GetStockObject PROTO :DWORD
RegisterClassA PROTO : DWORD
CreateWindowExA PROTO :DWORD ,: DWORD ,: DWORD ,: DWORD ,: DWORD ,: DWORD,:DWORD ,: DWORD ,: DWORD ,: DWORD ,: DWORD ,: DWORD
ShowWindow PROTO :DWORD ,: DWORD

.DATA
	hinst DWORD 0
	handleIcon DWORD 0
	handleCursor DWORD 0
	handleBrush DWORD 0
	hwnd DWORD 0

	wndc WNDCLASS <?>

	cname BYTE "MainClass", 0
	nazwa BYTE "Dupa",0

.CODE
WndProc PROC uses EBX ESI EDI windowHandle:DWORD, uMsg:DWORD, wParam:DWORD, lParam:DWORD
 
	INVOKE DefWindowProcA, windowHandle, uMsg, wParam, lParam

ret 
WndProc ENDP

main proc

	mov [wndc.clsStyle], CSstyle	
		
	INVOKE GetModuleHandleA, 0
	mov	hinst, EAX
	mov [wndc.clsLpfnWndProc], OFFSET WndProc
	mov [wndc.clsHInstance], EAX
	mov [wndc.clsCbClsExtra], 0
	mov [wndc.clsCbWndExtra], 0

	invoke LoadIconA, 0 , IDI_APPLICATION
	mov handleIcon, EAX
	mov [wndc.clsHIcon], EAX 

	invoke LoadCursorA , 0, IDC_ARROW
	mov handleCursor ,EAX
	mov [wndc.clsHCursor], EAX 

	invoke GetStockObject , WHITE_BRUSH
	mov handleBrush ,EAX
	mov [wndc.clsHbrBackground], EAX 

	mov [wndc.clsLpszMenuName], 0
	mov [wndc.clsLpszClassName], OFFSET cname

	INVOKE RegisterClassA , OFFSET wndc
;	mov cname, EAX
	INVOKE CreateWindowExA, 0, OFFSET cname, OFFSET nazwa, WNDstyle, 50, 50, 600, 400, 0, 0, hinst, 0
	mov hwnd, EAX
	invoke ShowWindow , hwnd , SW_SHOWNORMAL

	invoke ExitProcess, 0
main endp

END